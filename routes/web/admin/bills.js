var User = require('../../../models/user');
var Bill = require('../../../models/bill');

// var ObjectId = require('mongodb').ObjectId;
var uniqid = require("uniqid");
var moment = require("moment");
var mongoose = require("mongoose");

var billCategories = require("../../../config/types")["billCategories"];

module.exports = {


  get: function(req, res, next) {
    console.log(req.query);
    var societyId = req.session.user.societyId;

    var filter = {"societyId": societyId};
    var limit = 20; // If NO filters, show just 20

    if (req.query.block) {
      filter.block = req.query.block; //new RegExp(req.query.block, 'i');
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.flatno && req.query.flatno !== "==ANY==") {
      filter.flatNo = req.query.flatno; //new RegExp(req.query.flatno, 'i');
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.period) {
      var start = moment(req.query.period.split(' - ')[0], "DD/MM/YYYY").toDate();
      var end = moment(req.query.period.split(' - ')[1], "DD/MM/YYYY").toDate();
      filter.dueDate = {
        "$gte": start,
        "$lte": end
      };
      limit = null; // If a filter is active, there's no limit!
    }

    User.distinct("block", {societyId: societyId}, function (err, blocks) {
      if (err) {
        return res.json({error: true, reason: err});
      }
      Bill
      .find(filter)
      .limit(limit)
      .exec(function (err, bills) {
        if (err) {
          return res.json({error: true, reason: err});
        }
        return res.render('admin/bills', {
        // return res.json({
          title: 'Bills',
          error: false,
          blocks: blocks.sort().filter( b => {return b != '';} ),
          bills: bills,
          billCategories: billCategories,
          query: req.query,
          moment: moment
        });
      })

    })

  },

  post: function(req, res, next) {
    var data = req.body.formData;
    data.societyId = req.session.user.societyId;
    data.addedBy = req.session.user.id; // admin id
    data.dueDate = moment(data.dueDate, "DD/MM/YYYY").toDate(); // admin id
    // data.billNo = uniqid("BILL");

    /***** Single bill (Simplest case) **********/
    if (data.block !== "==ALL==" && data.flatNo !== "==ALL==") {
      User
      .findOne({role: "Resident", flatNo: data.flatNo, block: data.block, societyId: data.societyId })
      .exec(function (err, user) {
        data.billNo = uniqid("BILL");
        if (data.areaOrFixed === 'area') {
          data.amount = Number(data.rate * user.flatArea);
        } else {
          data.amount = Number(data.amount);
        }
        if (isNaN(data.amount)) {
          return res.json({error: true, reason: "Bill Amount must be Number (No Flat Area Mentioned?)"});
        }
        var bill = new Bill(data);
        bill.save(function(err) {
          if (err) {
            console.log(err);
            return res.json({
              error: true,
              message: "Fill the mandatory fields",
              reason: err
            });
          } else {
            res.json({
              error: false,
              data: {
                id: bill.id
              },
              refresh: true // prompt the UI to refresh immediately
            });
          }
        });
      })

/*****  Multiple Bills for all flats in all blocks (Basically for ALL users in a society) ****/
    } else if (data.block === "==ALL==") {
      User
      .aggregate([
        { "$match": {'role': "Resident", 'societyId': mongoose.Types.ObjectId(req.session.user.societyId)} },
        { "$group": {"_id": {block: "$block", flatNo: "$flatNo"}} }
      ])
      .then(function (results) { // results contains all the flatNo/Block groups
        // return res.json(results);
        results.forEach(function (result) {
          User
          .findOne({role: "Resident", flatNo: result._id.flatNo, block: result._id.block, societyId: data.societyId })
          .exec(function (err, user) {
            data.billNo = uniqid("BILL");
            data.block = result._id.block;
            data.flatNo = result._id.flatNo;
            if (data.areaOrFixed === 'area') {
              data.amount = Number(data.rate * user.flatArea);
            } else {
              data.amount = Number(data.amount);
            }
            if (isNaN(data.amount)) {
              // return res.json({error: true, reason: "Bill Amount must be Number!"});
              console.log("Bill Amount must be Number for ${data.block}, ${data.flatNo}!");
            }
            var bill = new Bill(data);
            bill.save(function(err) {
              if (err) {
                console.log(`Error saving bill for ${data.block}, ${data.flatNo} : ${err}`);
              } else {
                console.log(`saved bill for ${data.block}, ${data.flatNo}`);
              }
            });
          })

        })

      })
      .catch(function (err) {
        return res.json({error: true, reason: err});
      });
      return res.json({error: false, refresh: false}); // no waiting! No UI refresh

/**********  Multiple Bills for all flats in a particular block ***/
    } else if (data.flatNo === "==ALL==") {

      User.distinct("flatNo", {societyId: req.session.user.societyId, block: data.block}, function (err, flatNos) {
        if (err) {
          return res.json({error: true, reason: err});
        }
        flatNos.forEach(function (flatNo) {
          User
          .findOne({role: "Resident", flatNo: flatNo, block: data.block, societyId: data.societyId })
          .exec(function (err, user) {
            data.billNo = uniqid("BILL");
            data.flatNo = flatNo;
            if (data.areaOrFixed === 'area') {
              data.amount = Number(data.rate * user.flatArea);
            } else {
              data.amount = Number(data.amount);
            }
            if (isNaN(data.amount)) {
              return res.json({error: true, reason: "Bill Amount must be Number!"});
            }
            var bill = new Bill(data);
            bill.save(function(err) {
              if (err) {
                console.log(`Error saving bill for ${data.block}, ${data.flatNo} : ${err}`);
              }
            });
          })

        })
      })
      return res.json({error: false, refresh: false}); // no waiting! No UI refresh
    }


  },

  put: function(req, res, next) {
    var billId= req.params.billid;
    var data = req.body.formData;
    console.log(data.flatNo);
    data.dueDate = moment(data.dueDate, "DD/MM/YYYY").toDate();
    if (data.dueDate == 'Invalid Date') {
      return res.json({error: true, reason: "Invalid Due Date!"});
    }
    Bill
    .findOne({_id: billId})
    .exec()
    .then(function (bill2edit) {
      bill = bill2edit;
      console.log(bill);
      return User
      .findOne({flatNo: bill.flatNo, block: bill.block })
      .exec()
    })
    .then(function (user) {
      if (data.areaOrFixed === 'area') {
        data.amount = Number(data.rate * user.flatArea);
      } else {
        data.amount = Number(data.amount);
      }
      if (isNaN(data.amount)) {
        return res.json({error: true, reason: "Bill Amount must be Number!"});
      }

      bill.dueDate = data.dueDate;
      bill.desc = data.desc;
      bill.amount = data.amount;
      bill.block = (data.block !== undefined && data.block !== null && data.block !== '') ? data.block : bill.block;
      bill.flatNo = (data.flatNo !== undefined && data.flatNo !== null && data.flatNo !== '') ? data.flatNo : bill.flatNo;

      bill.save(function(err) {
        if (err) {
          console.log(err);
          return res.json({
            error: true,
            message: "Fill the mandatory fields",
            reason: err
          });
        } else {
          res.json({
            error: false,
            data: {
              id: bill.id
            }
          });
        }
      });
    })
    .catch(function (err) {
      return res.json({error: true, reason: err})
    })

  }


}
