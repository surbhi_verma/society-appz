var User = require('../../../models/user');
var Poll = require('../../../models/opinionpoll');
var Notification = require('../../../models/notification');

var moment = require('moment');

module.exports = {


  get: function (req, res) {
    var societyId = req.session.user.societyId;
    Poll.find({
      societyId: societyId
    })
    .sort({pollCreatedDate: -1})
    .exec(function (err, polls) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Polls could not be found"
        });
      } else {
        console.log(polls);
        res.render('admin/polls', {
          error: false,
          title: 'Opinion Polls',
          data: polls
        });
      }
    });
  },

  post: function (req, res) {
    console.log(req.body);
    var data = req.body;
    data.societyId = req.session.user.societyId;
    data.addedBy =  req.session.user.id;
    if (data.pollCloseDate) {
      data.pollCloseDate = moment(data.pollCloseDate, 'DD/MM/YYYY').toDate();
    } else {
      data.pollCloseDate = null;
    }
    var poll = new Poll(data);
    poll.save(function (err) {
      if (err) {
        console.log("oops %o",err);
        return res.json({ error: true, message: "Fill the mandatory fields " });
      } else {

        // raise notif & send emails
        Notification.addForSociety(data.societyId, "NEW POLL: "+data.question, function (err, docs) {
          if (err)
            console.log('error raising notifs for poll addition for society');
          else
            console.log('success raising notifs for poll addition for society');
        })
        // emails
        User.getEmailListForSociety(data.societyId, function (err, lst) {
          console.log(lst);
          var locals = {
            to: lst,
            // to: 's26c.sayan@gmail.com', // for testing
            subject: 'NEW POLL: ' + data.question,
            // noticeSubject: data.subject,
            question: data.question,
            options: data.options,
            endsOn: (data.pollCloseDate === null) ? null : moment(data.pollCloseDate).format("DD/MM/YYYY hh:mm A")
          }
          res.mailer.send('emails/poll', locals, function (err) {
            if (!err)
            console.log('Password emails sent successfully');
            //  res.send('done!');
            else {
              console.log("error sending password mail: %o", err);
              // res.send({error: true})
            }
          });
        })

        // ...but not wait for them to complete
        res.json({ error: false, data: { id: poll.id }
        });
      }
    });
  },


}
