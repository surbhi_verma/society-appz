var User = require('../../../models/user');
var Notice = require('../../../models/notice');
var moment = require('moment');

var Notification = require('../../../models/notification');


module.exports = {


  get: function(req, res, next) {
    var societyId = req.session.user.societyId;
    // console.log(societyId);
    User.find({societyId: societyId, role: "Resident"}, function (err, residents) {
      Notice.find({
        "societyId": societyId
      }, function(err, notices) {
        if (err) {
          console.log(err);
          return res.render('resident/notice', {
            error: true,
            message: "No notices found"
          });
        } else {
          var noticesMap = notices.map(function(notice) {
            var rObj = {};
            rObj['_id'] = notice._id;
            rObj['subject'] = notice.subject;
            rObj['content'] = notice.content;
            rObj['reminderDate'] = moment(notice.reminderDate).format('DD/MM/YYYY HH:mm');
            rObj['dateCreated'] = moment(notice.dateCreated).format('DD/MM/YYYY');
            rObj['isPublished'] = notice.isPublished;
            rObj['targetAt'] = notice.targetAt;
            return rObj;
          });
          console.log(noticesMap);
          res.render('admin/notices', {
            title: "Notices",
            error: false,
            data: noticesMap,
            moment: moment,
            targets: residents.map(function (resident) {
              return {block: resident.block, flatNo: resident.flatNo}
            })
          });
        }
      });
    })



  },

  post: function(req, res, next) {
    // console.log(req.body);
    // console.log(new ObjectId("5799ae6eb3b21ea20ff4eaf5"));
    var data = req.body;
    // console.log(data.reminderDate);
    if (data.reminderDate) {
      data.reminderDate = moment(data.reminderDate, 'DD/MM/YYYY HH:mm').toDate();
    }
    // else {
    //
    // }
    data.societyId = req.session.user.societyId;
    console.log(data);
    var notice = new Notice(data);
    notice.save(function(err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Fill the mandatory fields"
        });
      } else {
        // raise notif & send emails
        if (data.targetAt === undefined || data.targetAt === null) {
          Notification.addForSociety(data.societyId, "NOTICE: "+data.subject, function (err, docs) {
            if (err)
              console.log('error raising notifs for notice addition for society');
            else
              console.log('success raising notifs for notice addition for society');
          })
          // emails
          User.getEmailListForSociety(data.societyId, function (err, lst) {
            console.log(lst);
            var locals = {
              to: lst,
              // to: 's26c.sayan@gmail.com', // for testing
              subject: 'NOTICE:' + data.subject,
              // noticeSubject: data.subject,
              noticeContent: data.content
            }
            res.mailer.send('emails/notice', locals, function (err) {
              if (!err)
              console.log('Password emails sent successfully');
              //  res.send('done!');
              else {
                console.log("error sending password mail: %o", err);
                // res.send({error: true})
              }
            });
          })

        } else {
          Notification.addForFlat(data.targetAt.flatNo, data.targetAt.block, "NOTICE: "+data.subject, function (err, docs) {
            if (err)
              console.log('error raising notifs for notice addition for flat & block');
            else
              console.log('success raising notifs for notice addition for flat & block');
          })
          // emails
          User.getEmailListForFlat(data.societyId, data.targetAt.flatNo, data.targetAt.block, function (err, lst) {
            console.log(lst);
            var locals = {
              to: lst,
              // to: 's26c.sayan@gmail.com', // for testing
              subject: 'NOTICE:' + data.subject,
              // noticeSubject: data.subject,
              noticeContent: data.content
            }
            res.mailer.send('emails/notice', locals, function (err) {
              if (!err)
              console.log('Password emails sent successfully');
              //  res.send('done!');
              else {
                console.log("error sending password mail: %o", err);
                // res.send({error: true})
              }
            });
          })
        }
        // ...but not wait for them to complete
        res.json({
          error: false,
          data: {
            id: notice.id
          }
        });
      }
    });
  },

  put: function(req, res, next) {
    var data = req.body;
    if (data.reminderDate) {
      data.reminderDate = moment(data.reminderDate, 'DD/MM/YYYY HH:mm').toDate();
    }
    Notice.findByIdAndUpdate(data.id, data, {
      new: true
    }, function(err, notice) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Notice could not be updated"
        });
      } else {
        res.json({
          error: false,
          data: notice
        });
      }
    });
  }


}
